# README #

This README will help you to reproduce my experiment

### Instroduction ###

This report is going to reproduce the experiment in *What Happens When HTTP Adaptive streaming Players Compete for Bandwidth* with GENI testbed. The *goal* of this experiment is to verify that the assumptions made in paper can still arise in reality. It will take 1 hour.

### Background ###

as shown in Figure 1, adaptive video player works in *Steady-State* will request one Chunk every T seconds so that the player can be either ON, downloading a chunk, or it is OFF, staying idle. When two players competing limit bandwidth, Figure 1-a shows that when two players' ON periods do not overlap in T seconds, each player will measure the download speed as C, so that both players will choose video bitrates higher than fair share C/2 causing congestion then turn into Figure 1-b and Figure 1-c situations players will measure that the throughput is less than they measured before and switch back to lower video bitrates as chunk be smaller the ON periods back to situation Figure 1-a. This oscillatory can repeat, causing instability.

![1.png](https://bitbucket.org/repo/9kXrrB/images/1052101717-1.png)

*Figure 1*

### Results ###

![2.png](https://bitbucket.org/repo/9kXrrB/images/3174349107-2.png)

*Original Figure*

![figure_1.png](https://bitbucket.org/repo/9kXrrB/images/1551422602-figure_1.png)

*Reproduce Figure*

Compare the reproduce figure to the original, the trend is similar except in Number of active players part reproduce figure shows less details compare to original one, because during the experiment, I did not figure out a way to detect number of active players in channel very precisely, so I used the ON period and send request time to roughly determine the number of active players in period T seconds according to overlapping time.

### Run my experiment ###

* To start, create a new slice on the GENI portal. Create a client-server topology with two VMs connected with a link, by downloading and using this [RSpec](http://witestlab.poly.edu/repos/genimooc/run_my_experiment/dash_experiment/dash_rspec.xml).

![4.png](https://bitbucket.org/repo/9kXrrB/images/3358124277-4.png)

*Topology*

* download source under one folder, `cd /PATH/TO/folder` enter the folder.
* run command `scp -i /PATH/TO/id_rsa -P PORT /PATH/TO/file USERNAME@HOSTNAME:~/` transform `server.sh` and `bunny_Desktop.mpd` to your server node, `client.sh` and `RateBasedAdaptationLogic.cpp` and `dash_video_experiment.rb` to your client node, run command `chmod a+x XXX.sh`  to make them executable
* Login to your server and client VM, run `sudo su # if you are not logged in as root` run `./server.sh` and `./client.sh` to install software dependencies for this experiment, it will take a while. **if any error happen, you can try run commands in bash line by line.**
* test that everything works up until now. On client node execute
```
#!bash

vlc -I dummy -V dummy http://192.168.1.200/video/bunny_Desktop.mpd --oml-id client --oml-domain "VLC" --oml-collect "file:/dev/null" --quiet --oml-log-level 0 --sout "#duplicate{dst=display,dst=std{access=file,mux=ps,dst=/tmp/test.mp4}}" --dash-policy 1  
```
The output should be something like this


```
#!csv

VLC media player 2.1.0-git Rincewind (revision 1.3.0-git-6058-g2f0e36e)  
Feb 25 13:53:49 INFO    OML Client 2.11.0 [OMSPv5] Copyright 2007-2014, NICTA  
INFO    File_stream: opening local storage file '/dev/null'  
chosenRate_bps=101492 empiricalRate_bps=0 decisionRate_bps=0 buffer_percent=0  
chosenRate_bps=101492 empiricalRate_bps=0 decisionRate_bps=0 buffer_percent=0  
INFO    /dev/null: Connected  
chosenRate_bps=101492 empiricalRate_bps=5988064 decisionRate_bps=0 buffer_percent=0  
chosenRate_bps=101492 empiricalRate_bps=222384442 decisionRate_bps=0 buffer_percent=9  
chosenRate_bps=101492 empiricalRate_bps=208178498 decisionRate_bps=0 buffer_percent=13  
chosenRate_bps=101492 empiricalRate_bps=194267895 decisionRate_bps=0 buffer_percent=18  
chosenRate_bps=101492 empiricalRate_bps=171997078 decisionRate_bps=0 buffer_percent=23  
chosenRate_bps=5991271 empiricalRate_bps=196050159 decisionRate_bps=196050159 buffer_percent=30  
chosenRate_bps=5991271 empiricalRate_bps=208852276 decisionRate_bps=208852276 buffer_percent=39  
chosenRate_bps=5991271 empiricalRate_bps=650956631 decisionRate_bps=650956631 buffer_percent=59  
chosenRate_bps=5991271 empiricalRate_bps=886964692 decisionRate_bps=886964692 buffer_percent=74  
chosenRate_bps=5991271 empiricalRate_bps=922833287 decisionRate_bps=922833287 buffer_percent=77  
chosenRate_bps=5991271 empiricalRate_bps=962394258 decisionRate_bps=962394258 buffer_percent=80  
```
* Now in client node run `mv /users/**your_username**/RateBasedAdaptationLogic.cpp /root/vlc-2.1.0-git/modules/stream_filter/dash/adaptationlogic/RateBasedAdaptationLogic.cpp` to setup Simpler Player policy, in server node run `mv /users/**your_username**/bunny_Desktop.mpd /var/www/html/video/bunny_Desktop.mpd` to setup the video source and run `sudo tc qdisc replace dev eth1 root tbf rate 3mbit limit 200mb burst 32kB peakrate 3.01mbit mtu 1600` setup your channel bandwidth to 3mbit.
then run the following commands which will compile your new code and install the modified VLC application.

```
#!bash
sudo su #if you are not logged in as root
cd /root/vlc-2.1.0-git
./configure LIBS="-loml2" --enable-run-as-root --disable-lua --disable-live555 --disable-alsa --disable-dvbpsi --disable-freetype
make  
make install  
mv /usr/local/bin/vlc /usr/local/bin/vlc_app  
echo '#!/bin/sh' > /usr/local/bin/vlc  
echo 'export LD_LIBRARY_PATH="/usr/local/lib${LD_LIBRARY_PATH:+:}${LD_LIBRARY_PATH}"' >> /usr/local/bin/vlc  
echo 'export TERM=xterm' >> /usr/local/bin/vlc  
echo 'vlc_app "$@"' >> /usr/local/bin/vlc  
chmod +x /usr/local/bin/vlc  
```

* Open two new terminal, log in to your client node, one both terminal run command

```
#!bash

vlc -I dummy -V dummy http://192.168.1.200/video/bunny_Desktop.mpd --oml-id client --oml-domain "VLC" --oml-collect "file:/dev/null" --quiet --oml-log-level 0 --sout "#duplicate{dst=display,dst=std{access=file,mux=ps,dst=/tmp/test.mp4}}" --dash-policy 2
```
after about 80 seconds use Ctrl+c to stop the process and copy the data from terminal and save as **dash_out_4.csv** and **dash_out_2.csv** on your local pc separately. 

*  run `python analyze.py` to generate the figure.

### Notes ###

Experiment environment:

* *Ubuntu 14.04.4 LTS*
* *Python 2.7.6* include [*numpy*](https://www.scipy.org/install.html) and [*matplotlib*](http://matplotlib.org/users/installing.html) module
* *NYU InstaGENI*

Helpful sources:

* *[Adaptive video policies for DASH video](http://witestlab.poly.edu/blog/adaptive-video/)*
* *[Vlc-2.2.2-git source code](https://fossies.org/dox/vlc-2.2.2/index.html)*
* *[What Happens When HTTP Adaptive Streaming Players Compete for Bandwidth?](https://pdfs.semanticscholar.org/3f62/3aed8de2c45820eab2e4591b1207b3a65c13.pdf)*

**Thanks to GENI testbed, support this experiment perfectly**