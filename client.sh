cd ~  
sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/tools:/mytestbed:/stable/xUbuntu_12.04/ /' >> /etc/apt/sources.list.d/oml2.list"  
apt-get update  
apt-get -y build-dep vlc  
apt-get -y --force-yes install subversion liboml2-dev
wget https://www.freedesktop.org/software/vaapi/releases/libva/libva-1.1.1.tar.bz2  
tar -xjvf libva-1.1.1.tar.bz2  
cd libva-1.1.1  
./configure
make  
make install  
ldconfig  
cd ~  
svn co http://witestlab.poly.edu/repos/genimooc/dash_video/vlc-2.1.0-git  
cd vlc-2.1.0-git  
./configure LIBS="-loml2" --enable-run-as-root --disable-lua --disable-live555 --disable-alsa --disable-dvbpsi --disable-freetype
make  
make install  
mv /usr/local/bin/vlc /usr/local/bin/vlc_app  
echo '#!/bin/sh' > /usr/local/bin/vlc  
echo 'export LD_LIBRARY_PATH="/usr/local/lib${LD_LIBRARY_PATH:+:}${LD_LIBRARY_PATH}"' >> /usr/local/bin/vlc  
echo 'export TERM=xterm' >> /usr/local/bin/vlc  
echo 'vlc_app "$@"' >> /usr/local/bin/vlc  
chmod +x /usr/local/bin/vlc