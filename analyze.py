import numpy as np
import matplotlib.pyplot as plt

fig1 = plt.figure(1)
data1 = np.loadtxt('dash_out_2.csv')
data2 = np.loadtxt('dash_out_4.csv')
ax1 = fig1.add_subplot(111)
ax2 = ax1.twinx()
temp = data1[0][1]
for i in range(len(data1)):
    data1[i][1] = data1[i][1] - temp
arry1 = np.zeros(len(data1))
arry2 = np.zeros(len(data1))
for i in range(len(data1)):
    arry1[i] = data1[i][1]
    arry2[i] = data1[i][3]/1000000.0 
l1 = ax1.plot(arry1, arry2, 'r', label = r'Player-1 chunk throughput')
temp = data2[0][1]
for i in range(len(data2)):
    data2[i][1] = data2[i][1] - temp
arry1 = np.zeros(len(data2))
arry2 = np.zeros(len(data2))
for i in range(len(data2)):
    arry1[i] = data2[i][1]
    arry2[i] = data2[i][3]/1000000.0 
l2 = ax1.plot(arry1, arry2, 'g--', label = r'Player-2 chunk throughput')
for i in range(len(data1)):
    arry1[i] = data1[i][1]
    arry2[i] = data1[i][2]/1000000.0
l3 = ax1.plot(arry1, arry2, 'b+', label = r'Player-1 requseted bitrate')
for i in range(len(data2)):
    arry1[i] = data2[i][1]
    arry2[i] = data2[i][2]/1000000.0 
l4 = ax1.plot(arry1, arry2, 'c*', label = r'Player-2 requseted bitrate')
x = np.linspace(0, 80)
y = np.linspace(1.5, 1.5)
l5 = ax1.plot(x, y, 'b', label = r'Fair share of the avail-bw')
for i in range(min(len(data1), len(data2))):
    arry1[i] = data1[i][1]
    if data1[i][4] + data2[i][4] < 0.7:
        arry2[i] = 1
    else:
        arry2[i] = 2
l6 = ax2.plot(arry1, arry2, 'k', label = r'Number of active players')
ax1.legend(loc = 'upper left')
ax2.legend(loc = 'upper right')
ax1.set_xlim(0, 80)
ax1.set_ylim(0, 4)
ax2.set_xlim(0, 80)
ax2.set_ylim(0.5, 2.5)
ax1.set_ylabel('Mbps')
ax2.set_ylabel('Number of active players')
ax2.set_xlabel('Time(seconds)')
plt.show()
