 #!/usr/bin/env ruby

require 'open3'  
require 'csv'

POLICY      = ARGV[0]  
VIDEO_OUT   = "/tmp/test_#{POLICY}.mp4"  
parsed_data = []  
tokens      = ["a","b","c","d"]

vlc_cmd = "vlc -I dummy -V dummy http://192.168.1.200/video/bunny_Desktop.mpd --oml-id client --oml-domain 'VLC' --oml-collect 'file:/dev/null' --quiet --oml-log-level 0 --sout '#duplicate{dst=display,dst=std{access=file,mux=ps,dst=#{VIDEO_OUT}}}' --dash-policy #{POLICY}"

def write_data(data)  
  fname   = "./dash_out_#{POLICY}.csv"
  i       = 1
  sum     = 0
  avg     = 0
  freezes = 0
  r1      = nil
  var_sum = 0
  var     = 0

  CSV.open(fname, "wb") do |csv|
    data.each do |d|
      next if d.empty?
      sum += d["a"]
      avg = sum / i
      unless r1.nil?
        var_sum += Math.log(d["a"]) - Math.log(r1)
        var = var_sum / (i - 1) 
      end
      r1 = d["a"]
      csv << [i, d["a"], d["b"], d["c"], d["d"]]
      freezes += 1 if d["a"] == 0

      i += 1
    end
  end

  [fname, avg, var, freezes]
end

begin  
  stdin, stdout, stderr = Open3.popen3(vlc_cmd)
  while data = stderr.gets
    puts data
    pdata = {}
    tokens.each do |token|
      split_tok = data.split("#{token}=")
      if split_tok && split_tok[1]
        left_part = split_tok[1]
        pdata[token] = left_part.split(' ')[0].to_i if left_part.split(' ') && left_part.split(' ')[0]
      end
    end
    parsed_data << pdata
  end
rescue Interrupt => e  
  `pkill -9 vlc`
  puts "\nThe video output is: #{VIDEO_OUT}"
  print "Saving data to file: "
  fname, avg, var, freezes = write_data(parsed_data)
  puts fname
  puts "The overall average video rate is:    #{avg}"
  puts "The variability of the video quality: #{var}"
  puts "The overall number of freezes are:    #{freezes}"
end  
